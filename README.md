# OpenML dataset: elevators

https://www.openml.org/d/43992

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "classification on numerical features" benchmark. Original description: 
 
This data set is also obtained from the task of controlling a F16
aircraft, although the target variable and attributes are different
from the ailerons domain. In this case the goal variable is related to
an action taken on the elevators of the aircraft.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43992) of an [OpenML dataset](https://www.openml.org/d/43992). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43992/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43992/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43992/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

